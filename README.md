# PyPoly

_A simple and educational example of how to define polynomials over fields in the Python programming language._

Implementing a class that models polynomials over fields contain a lot of cool features from python. You end up implementing dunder 
methods to give to the user the possibility to employ the well know arithmetic operators +, -, * and / when dealing with operations
involving polynomials, typing annotation of classes, static methods, class methods, abstract classes to define the interface that a
custom class representing a field must have, unit tests and so on.

## How to use?

First of all, create an virtual environment with python 3.9 if you're using an earlier version. 
Now, in its simple form, pass a dict with the powers in the keys and the corresponding coefficients as the values.

```python
from polynomial import Polynomial

p = Polynomial.fromDict({1: 2.2, 0: 3})
```

When you perform operations on such polynomial, all its coefficients will be floats. If you wish, instead, to use coefficients of a 
particular field, for example, rationals or complex numbers, there're some implementations under the field module. Let's create a 
polynomial with complex coefficients

```python
from field.complex import Complex
from polynomial import Polynomial

p = Polynomial.fromDict({1: Complex(2, 1), 0: Complex(0, 1)})
```

If you call `print(p)`, it should print the corresponding polynomial

```
(2 + 1i)x^1 + i
```

When you perform an operation over the polynomial p, such as multiplication `p*p`, you'll see that the result have all its coefficients
as complex numbers. Let's call `print(p*p)`,

```
(3 + 4i)x^2 + (-1 + 2i)x^1 + -1
```

## Cool stuff to learn!

Implementing a polynomial class with coefficients belonging to fields is a simple example where we can use a lot of the more advanced
python machinery!

### Dunder methods

The Polynomial class defines a lot of dunder methods. These methods are treated as special by the Python interpreter. One such case is the
method `__add__` as follows

```python
    def __add__(self, p: 'Polynomial') -> 'Polynomial':
        
        terms = self.terms.copy()
        for degree, monomial in p.terms.items():
            terms.update({degree: terms[degree].__add__(monomial) if degree in terms else monomial})
 
        return Polynomial(terms.values())
```

that allows us to write `p1 + p2`, with both `p1` and `p2` being polynomials. Another one is the operator `-`, that converts the sign of the
coefficients of a polynomial,

```python
    def __neg__(self):
        return Polynomial([-i for i in self.terms.values()])
```

One cool thing is that some methods can be easily written with the help of another dunder methods, such as subtraction that is simply

```python
    def __sub__(self, p: 'Polynomial') -> 'Polynomial':
        return self.__add__(p.__neg__())
```

Notice how we just use the unary negative `__neg__` operator and the `__add__` method. These two methods also show us that we are annotating
them to indicate that ther return Polynomials. As we annotate a class method returning itself, we have to use the string `'Polynomial'`.

### Class methods

Another point is that we can include two `classmethods`, such that we can instantiate the class either from a `dict` or from a `list`. Class
methods are, in this example, _shortcuts_ to initialize a class from different sources.

```python
    @classmethod
    def fromDict(cls, terms: dict[int: Field]):
        return cls([Monomial(coeff, degree) for degree, coeff in terms.items()])

    @classmethod
    def fromList(cls, terms: list[tuple[int, Field]]):
        return cls([Monomial(i[1], i[0]) for i in terms])
```

### Static methods

When printing our polynomial, we want it to be ordered. Therefore in the method `__repr__` we have to sort the dictionary. How do we do it?
We can employ the built-in function `sorted` but we still have to tell it how to sort our elements. Well, ranking has to be made by the degree of each monomial and therefore we pass a function that simply returns the degree. Now comes the question, where do we put this function? 

This function share no state with the class, which makes us tempting to put it outside the class. But it also conceivable that it belongs in the class, it's closely connect to the `__repr__` method. Moreover, inside the class, it helps the overall code organization. What we can do is to decorate the _rank method with a `@staticmethod` to inform that it's inside the class but doesn't share its state.

```python
    @staticmethod
    def _rank(term: Monomial):
        return term.n
```

Note the `_` ahead of the method name. This is a convention to signal that this method is private and should not be used outside the class!

## Abstract classes

The polynomial, as defined on its own, is independent of the coefficients used. You can employ floats or ints and it should work. However, if
we want, we can build custom classes to define fields of numbers and the polynomial coefficients will always remain within this field when
we apply the common operations of +,-,*, and /. 
It's also useful because we can define a representation of these numbers, by the `__repr__` dunder method, that python will use every time it prints something related to the particular number. 

We expect that these custom classes have a minimum set of methods implemented. They must tell python how to perform, at least, the basic four arithmetic operations. To declare the interface that is required, we can use an abstract class such as the following

```python
from abc import ABC, abstractmethod

class Field(ABC):

    @abstractmethod
    def __add__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __mul__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __truediv__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __neg__(self) -> 'Field':
        pass

    @abstractmethod
    def __sub__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __str__(self) -> str:
        pass
```

And the implementation of a field must inherit from it, such as the Rational class

```python
from field.field import Field

class Rational(Field):
```

If any of the methods defined on the abstract class is missing, python will throw an error.