from field.field import Field
from math import sqrt

class Complex(Field):

    def __init__(self, real, imaginary):
        self.real = real
        self.imaginary = imaginary

    @classmethod
    def unit(cls, x):
        return Complex(1, 0) 

    def __add__(self, that: 'Complex') -> 'Complex': 
        return Complex(self.real + that.real, self.imaginary + that.imaginary)   

    def __mul__(self, that: 'Complex') -> 'Complex':
        real = self.real*that.real - self.imaginary*that.imaginary
        imaginary = self.real*that.imaginary + self.imaginary*that.real
        return Complex(real, imaginary)  

    def conj(self) -> 'Complex':
        return Complex(self.real, -self.imaginary)

    def scalar_mult(self, x) -> 'Complex':
        return Complex(x*self.real, x*self.imaginary)

    def __abs__(self) -> float:
        return sqrt(self.real**2 + self.imaginary**2)

    def __truediv__(self, that: 'Complex') -> 'Complex':
        return Complex(self.real, self.imaginary)*that.conj().scalar_mult(1/abs(that)**2)  

    def __neg__(self):
        return Complex(-self.real, -self.imaginary)

    def __sub__(self, that: 'Complex') -> 'Complex':
        return self.__add__(that.__neg__())

    def __str__(self) -> str:
        if not self.real:
            return f"{self.imaginary}i" if self.imaginary != 1 else "i"
        if not self.imaginary:
            return f"{self.real}"

        return f"({self.real} + {self.imaginary}i)"