from abc import ABC, abstractmethod

class Field(ABC):

    @abstractmethod
    def __add__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __mul__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __truediv__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __neg__(self) -> 'Field':
        pass

    @abstractmethod
    def __sub__(self, that: 'Field') -> 'Field':
        pass

    @abstractmethod
    def __str__(self) -> str:
        pass