from field.field import Field

class Rational(Field):

    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator

    @classmethod
    def unit(cls, x):
        return Rational(x, 1) 

    def __add__(self, that: 'Rational') -> 'Rational': 
        top = self.numerator*that.denominator + self.denominator*that.numerator
        bottom = self.denominator*that.denominator
        return Rational(top, bottom)   

    def __mul__(self, that: 'Rational') -> 'Rational':
        top = self.numerator*that.numerator
        bottom = self.denominator*that.denominator
        return Rational(top, bottom)  

    def __truediv__(self, that: 'Rational') -> 'Rational':
        top = self.numerator*that.denominator
        bottom = self.denominator*that.numerator
        return Rational(top, bottom)  

    def __neg__(self):
        return Rational(-self.numerator, self.denominator)

    def __sub__(self, that: 'Rational') -> 'Rational':
        return self.__add__(that.__neg__())

    def __str__(self) -> str:
        return f"{self.numerator}/{self.denominator}"