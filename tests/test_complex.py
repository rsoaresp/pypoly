import pytest
from field.complex import Complex

class TestBasicNumbers:

    @pytest.mark.parametrize("a, b, expected", [(1,0,1), (0,0,0), (0,1,0), (1,1,1), (2,3,2)])
    def test_real_part(self, a, b, expected):
        c = Complex(a, b)
        assert c.real == expected

    @pytest.mark.parametrize("a, b, expected", [(1,0,0), (0,0,0), (0,1,1), (1,1,1), (2,3,3)])
    def test_imaginary_part(self, a, b, expected):
        c = Complex(a, b)
        assert c.imaginary == expected

class TestOperations:

    ADD = [((1,0), (0,1), (1,1)), ((1,1), (-1,-1), (0, 0)), ((0,1), (2,3), (2,4))] 
    SUB = [((1,0), (0,1), (1,-1)), ((1,1), (-1,-1), (2, 2)), ((0,1), (2,3), (-2,-2))] 
    MUL = [((1,0), (1,0), (1,0)), ((0,1),(0,1),(-1,0)), ((1,0),(0,1), (0,1)), ((2,3),(5,7),(-11,29))]
    DIV = [((1,0), (1,0), (1,0)), ((0,1),(0,1),(1,0)), ((1,0),(0,1), (0,-1)), ((0,1),(1,0),(0,1))]

    @pytest.mark.parametrize("a, b, expected", ADD)
    def test_add(self, a, b, expected):
        c = Complex(*a) + Complex(*b)
        expected = Complex(*expected)
        assert c.real == expected.real and c.imaginary == expected.imaginary

    @pytest.mark.parametrize("a, b, expected", SUB)
    def test_sub(self, a, b, expected):
        c = Complex(*a) - Complex(*b)
        expected = Complex(*expected)
        assert c.real == expected.real and c.imaginary == expected.imaginary

    @pytest.mark.parametrize("a, b, expected", MUL)
    def test_mul(self, a, b, expected):
        c = Complex(*a)*Complex(*b)
        expected = Complex(*expected)
        assert c.real == expected.real and c.imaginary == expected.imaginary

    @pytest.mark.parametrize("a, b, expected", DIV)
    def test_div(self, a, b, expected):
        c = Complex(*a)/Complex(*b)
        expected = Complex(*expected)
        assert c.real == expected.real and c.imaginary == expected.imaginary


class TestComplexNumberProperties:

    TESTS_ABS = [((1,0), 1), ((0,1), 1), ((3,4), 5)]
    TESTS_CONJ = [((1,0), (1,0)), ((0,1), (0,-1)), ((1,1), (1,-1))]

    @pytest.mark.parametrize("a, expected", TESTS_ABS)
    def test_abs(self, a, expected):
        assert abs(Complex(*a)) == expected

    @pytest.mark.parametrize("a, expected", TESTS_CONJ)
    def test_conj(self, a, expected):
        assert Complex(*a).conj().imaginary == Complex(*expected).imaginary