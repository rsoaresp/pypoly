
class Monomial:

    def __init__(self, p, n):
        self.p = p
        self.n = n

    def __add__(self, that) -> 'Monomial':
        if that.n == self.n:
            return Monomial(that.p + self.p, self.n)
        else:
            raise ArithmeticError("Error ")

    def scalar_mult(self, factor) -> 'Monomial':
        return Monomial(factor*self.p, self.n)

    def __neg__(self) -> 'Monomial':
        return Monomial(-self.p, self.n)

    def __sub__(self, that: 'Monomial') -> 'Monomial':
        return self.__add__(that.__neg__())

    def __mul__(self, that: 'Monomial') -> 'Monomial':
        return Monomial(self.p*that.p, self.n + that.n)

    def __truediv__(self, that: 'Monomial') -> 'Monomial':
        return Monomial(self.p/that.p, self.n - that.n)

    def derivative(self) -> 'Monomial':
        return Monomial(self.p*self.n, self.n - 1)

    def integral(self) -> 'Monomial':
        return Monomial(self.p/(1 + self.n), self.n + 1)

    def __repr__(self):
        return f"{self.p}x^{self.n}" if self.n != 0 else f"{self.p}"