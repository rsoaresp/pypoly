from field.field import Field
from monomial import Monomial
from typing import Optional


class Polynomial:

    def __init__(self, terms: Optional[list[Monomial]] = None):
        self.terms = {i.n: i for i in terms} if terms else dict()

    @classmethod
    def fromDict(cls, terms: dict[int: Field]):
        return cls([Monomial(coeff, degree) for degree, coeff in terms.items()])

    @classmethod
    def fromList(cls, terms: list[tuple[int, Field]]):
        return cls([Monomial(i[1], i[0]) for i in terms])

    def __add__(self, p: 'Polynomial') -> 'Polynomial':
        
        terms = self.terms.copy()
        for degree, monomial in p.terms.items():
            terms.update({degree: terms[degree].__add__(monomial) if degree in terms else monomial})
 
        return Polynomial(terms.values())

    def __getitem__(self, i):
        return self.terms[i]

    def __neg__(self):
        return Polynomial([-i for i in self.terms.values()])

    def __sub__(self, p: 'Polynomial') -> 'Polynomial':
        return self.__add__(p.__neg__())

    def __mul__(self, p: 'Polynomial') -> 'Polynomial':
        products = list()
        for i in self.terms.values():
            for j in p.terms.values():
                products.append(i.__mul__(j))
        return Polynomial(products) 

    def scalar_mult(self, factor) -> 'Polynomial':
        return Polynomial(monomials=[i.scalar_mult(factor) for i in self.terms.values()])
    
    def integral(self) -> 'Polynomial':
        return Polynomial(monomials=[i.integral() for i in self.terms.values()])

    def derivative(self) -> 'Polynomial':
        return Polynomial(monomials=[i.derivative() for i in self.terms.values()])

    @staticmethod
    def _rank(term: Monomial):
        return term.n

    def __repr__(self) -> str:
        return ' + '.join([i.__str__() for i in sorted(self.terms.values(), reverse=True, key=self._rank)])